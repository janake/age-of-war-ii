package org.prodet.players.player1;

import org.prodet.configuration.Context;
import org.prodet.configuration.Position3D;
import org.prodet.players.King;
import org.prodet.soldier.Soldier;

public class Sith extends Soldier {

    public Sith(Position3D pos, Context context, String team, int minStamina,
                int minStrength, int minSpeed, String rank, King king, Position3D targetPos) {
        super(pos.getxPos(), pos.getyPos(), context, team,
                minStamina, minStrength, minSpeed, rank,
                targetPos, king);
    }

    @Override
    public boolean inTheFinish(Position3D pos) {
//        if (pos.getxPos() > 950) {
//            return true;
//        } else {
//            return false;
//        }
        return isMyKingDied();
    }

}
