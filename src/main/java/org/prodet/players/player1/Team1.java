package org.prodet.players.player1;

import org.prodet.configuration.Context;
import org.prodet.configuration.Position3D;
import org.prodet.players.King;
import org.prodet.soldier.Soldier;
import org.prodet.soldier.SoldiersFactory;
import org.prodet.soldier.formation.Triangle;

import java.util.Set;

public class Team1 {

    public static final String TEAM_1 = "TEAM1";
    private King kingB;
    int teamBXPos = 70;
    int teamBYpos = 5;

    public Team1(Context context) {
        kingB = new King(teamBXPos - 20, teamBYpos + 10, context, TEAM_1, "Sith King ");
    }

    public void createTeam1(Context context, Set<Soldier> soldiers, Position3D targetPos) {
        soldiers.add(kingB);
        SoldiersFactory.createSith(soldiers, 100, teamBXPos, teamBYpos,
                TEAM_1, context, 50, 20,
                10, "Sith Lord ", new Triangle(), kingB, targetPos);
    }

    public Position3D getKingPos() {
        return new Position3D(kingB.getxPos(), kingB.getyPos(), 1);
    }
}
