package org.prodet.players.player2;

import org.prodet.configuration.Context;
import org.prodet.configuration.Position3D;
import org.prodet.players.King;
import org.prodet.soldier.Soldier;
import org.prodet.soldier.SoldiersFactory;
import org.prodet.soldier.formation.Triangle;

import java.util.Set;

public class Team2 {

    public static final String TEAM_2 = "TEAM2";
    private King kingA;
    int teamAXPos = 0;
    int teamAYPos = 5;

    public Team2(Context context) {
        teamAXPos = context.getWidth() - 100;
        kingA = new King(teamAXPos + 60, teamAYPos + 10, context, TEAM_2, "Jedi King ");
    }

    public void createTeam2(Context context, Set<Soldier> soldiers, Position3D targetPos) {
        soldiers.add(kingA);
        SoldiersFactory.createJedi(soldiers, 100, teamAXPos, teamAYPos,
                TEAM_2, context, 50, 20, 10,
                "Jedi Master ", new Triangle(), kingA, targetPos);
    }

    public Position3D getKingPos() {
        return new Position3D(kingA.getxPos(), kingA.getyPos(), 1);
    }
}
