package org.prodet.players;

import org.prodet.configuration.Context;
import org.prodet.configuration.Position3D;
import org.prodet.soldier.Soldier;

import java.util.HashSet;
import java.util.Set;

public class King extends Soldier {

    Set<Soldier> subordinates = new HashSet<>();

    public King(int xPos, int yPos, Context context, String team, String rank) {
        super(xPos, yPos, context, team, 100, 40, 0, rank, new Position3D(xPos, yPos, 1), null);
    }

    @Override
    public Position3D move(int posX, int posY, int posZ, int speed) {
        return new Position3D(this.getxPos(), this.getyPos(), this.getxPos());
    }

    @Override
    public boolean inTheFinish(Position3D pos) {
        return false;
    }

    @Override
    protected void messageAboutADeath() {
        System.out.println("The great emperor " + this.getSoldierName() + " has died. ");
    }

    @Override
    protected void ifIDied() {
//  kill allordinate
//        subordinates.stream().filter(
//                s -> s.getTeam().equals(getTeam())).forEach( s -> {
//            s.interrupt();
//        });

//GAME OVER
        System.exit(0);
    }
}
