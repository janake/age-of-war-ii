package org.prodet.frontend;

import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.shape.CullFace;
import javafx.scene.shape.Sphere;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import org.prodet.configuration.Context;
import org.prodet.soldier.Soldier;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class UI3D extends Thread {

    private final Stage primaryStage;
    private final Context config;
    private final Group group = new Group();
    private final Map<Long, Sphere> spheres;

    public UI3D(Context config, Stage primaryStage) {
        this.config = config;
        this.primaryStage = primaryStage;
        boolean fixedEyeAtCameraZero = false;

        PerspectiveCamera camera = new PerspectiveCamera(fixedEyeAtCameraZero);
        camera.setTranslateX(0);
        camera.setTranslateY(config.getHeight()/2);
        camera.setTranslateZ(-300);

        Rotate rz = new Rotate(45.0, Rotate.X_AXIS);
        camera.getTransforms().add(rz);

        Scene scene = new Scene(group, config.getWidth(), config.getHeight(), true);
        primaryStage.setY(380);
        primaryStage.setX(50);
        scene.setCamera(camera);
        primaryStage.setScene(scene);
        this.primaryStage.show();

        spheres = config
                .getSoldiers()
                .stream()
                .collect(Collectors.toConcurrentMap(
                        s -> s.getId(),
                        s ->  createSphereOnMap(s.getxPos(), s.getyPos(), 5)));

        group.getChildren().addAll(spheres.values());
    }

    @Override
    public void run() {

        while(true){
            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(
                    () -> {
                            Map<Long, Sphere> elementsToDropOff = new HashMap<>();
                            spheres.entrySet().forEach( c -> {
                                long id = c.getKey();
                                Optional<Soldier> soldierById = config.getSoldierById(id);
                                if (soldierById.isPresent()) {
                                    moveCircleToNextField(c, soldierById);
                                } else {
                                    elementsToDropOff.put(c.getKey(), c.getValue());
                                }
                            });
                            elementsToDropOff.forEach((k, v) -> {
                                spheres.remove(k);
                                group.getChildren().remove(v);
                            });
                    });
        }
    }

    private void moveCircleToNextField(Map.Entry<Long, Sphere> c, Optional<Soldier> soldierById) {
        Soldier soldier = soldierById.get();
        c.getValue().setTranslateX(soldier.getxPos());
        c.getValue().setTranslateY(soldier.getyPos());
    }

    private Sphere createSphereOnMap(int x, int y, int radius) {
        Sphere sphere = new Sphere(radius);
        sphere.setCullFace(CullFace.NONE);
        sphere.setTranslateX(x);
        sphere.setTranslateY(y);
        sphere.setTranslateZ(-radius*2);
        return sphere;
    }
}
