package org.prodet.frontend;

import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import org.prodet.configuration.Context;
import org.prodet.soldier.Soldier;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class UI2D extends Thread {

    private final Stage primaryStage;
    private final Context config;
    private final Group group = new Group();
    private final Map<Long, Circle> circles;

    public UI2D(Context config, Stage primaryStage) {
        this.config = config;
        this.primaryStage = primaryStage;
        Scene scene = new Scene(group, config.getWidth(), config.getHeight());
        primaryStage.setY(10);
        primaryStage.setX(50);
        primaryStage.setScene(scene);
        this.primaryStage.show();

        circles = config
                .getSoldiers()
                .stream()
                .collect(Collectors.toConcurrentMap(
                        s -> s.getId(),
                        s ->  createPointOnMap(s.getxPos(), s.getyPos(), 1)));

        group.getChildren().addAll(circles.values());
    }

    @Override
    public void run() {

        while(true){
            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(
                    () -> {
                            Map<Long, Circle> elementsToDropOff = new HashMap<>();
                            circles.entrySet().forEach( c -> {
                                long id = c.getKey();
                                Optional<Soldier> soldierById = config.getSoldierById(id);
                                if (soldierById.isPresent()) {
                                    moveCircleToNextField(c, soldierById);
                                } else {
                                    elementsToDropOff.put(c.getKey(), c.getValue());
                                }
                            });
                            elementsToDropOff.forEach((k, v) -> {
                                circles.remove(k);
                                group.getChildren().remove(v);
                            });
                    });
        }
    }

    private void moveCircleToNextField(Map.Entry<Long, Circle> c, Optional<Soldier> soldierById) {
        Soldier soldier = soldierById.get();
        c.getValue().setCenterX(soldier.getxPos());
        c.getValue().setCenterY(soldier.getyPos());
    }

    private Circle createPointOnMap(int x, int y, int radius) {
        Circle circle = new Circle();
        circle.setCenterX(x);
        circle.setCenterY(y);
        circle.setRadius(radius);
        return circle;
    }
}
