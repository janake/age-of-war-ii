package org.prodet.configuration;

import org.prodet.soldier.Soldier;

import java.util.HashSet;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Context {

    public static Random RND = new Random();
    private int width = 960;
    private int height = 300;
    private long[][] battleField = new long[width][height];
    private Lock lock = new ReentrantLock();

    private Set<Soldier> soldiers;
    private Set<Soldier> survivedSoldiers = new CopyOnWriteArraySet<>();
    private volatile int columns;
    private volatile int offset;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public long getBattleFieldValue(int posX, int posY) {
        lock.lock();
        try {
            return battleField[posX][posY];
        } finally {
            lock.unlock();
        }
    }

    public void setValueOnBattleField(int xPos, int yPos, long newValue) {
        lock.lock();
        try {
            battleField[xPos][yPos] = newValue;
        } finally {
            lock.unlock();
        }
    }

    public void setIdsToBattleField(Set<Soldier> soldiers) {
        soldiers.forEach(s -> setValueOnBattleField(s.getxPos(), s.getyPos(), s.getId()));
    }

    public void setSoldiers(Set<Soldier> soldiers) {
        this.soldiers = soldiers;
    }

    public Set<Soldier> getSoldiers() {
        return new HashSet<>(soldiers);
    }

    public Optional<Soldier> getSoldierById(long id) {
        return soldiers.stream().filter(
                s -> s.getId() == id)
                .findFirst();
    }

    public boolean existingPosition(int xPos, int yPos, int zPos){
        return xPos<this.width && yPos< this.height;
    }

    public boolean existingPosition(Position3D position){
        return  position.getxPos()< this.width && position.getyPos()<this.height;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getColumns() {
        return columns;
    }

    public int getOffset() {
        return offset;
    }

    public Set<Soldier> getSurvivedSoldiers() {
        return survivedSoldiers;
    }

    public void setSurvivedSoldiers(Set<Soldier> survivedSoldier) {
        this.survivedSoldiers = survivedSoldiers;
    }

    public void addSurvivedSoldiers(Soldier soldier) {
        survivedSoldiers.add(soldier);
    }

    public void removeSoldier(Soldier soldier) {
        soldiers.remove(soldier);
    }
}
