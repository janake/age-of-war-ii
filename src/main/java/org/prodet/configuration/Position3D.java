package org.prodet.configuration;

public class Position3D {

    private int xPos;
    private int yPos;
    private int zPos;

    public Position3D(int xPos, int yPos, int zPos) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.zPos = zPos;
    }

    public int getxPos() {
        return xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public int getzPos() {
        return zPos;
    }

}
