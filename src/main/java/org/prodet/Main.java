package org.prodet;

import javafx.application.Application;
import javafx.application.ConditionalFeature;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.prodet.configuration.Context;
import org.prodet.players.King;
import org.prodet.players.player1.Team1;
import org.prodet.players.player2.Team2;
import org.prodet.soldier.SoldiersFactory;
import org.prodet.frontend.UI2D;
import org.prodet.frontend.UI3D;
import org.prodet.soldier.*;
import org.prodet.soldier.formation.Triangle;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class Main extends Application {

    @Override
    public void start(Stage twoDStage) {
        Context context = new Context();
        Stage threeDStage = new Stage();
        Set<Soldier> soldiers = new CopyOnWriteArraySet<>();

        Team1 t1 = new Team1(context);
        Team2 t2 = new Team2(context);
        t1.createTeam1(context, soldiers, t2.getKingPos());
        t2.createTeam2(context, soldiers, t1.getKingPos());

        context.setIdsToBattleField(soldiers);
        context.setSoldiers(soldiers);

        UI2D ui2D = new UI2D(context, twoDStage);
        ui2D.start();

        if (Platform.isSupported(ConditionalFeature.SCENE3D)) {
            UI3D ui3D = new UI3D(context, threeDStage);
            ui3D.start();
        }

        (checkResult(context)).start();

        for (Soldier s: context.getSoldiers()) {
            s.start();
        }
    }

    private Thread checkResult(Context context) {
        Thread t1 = new Thread( () -> {
            while (true){
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(context.getSoldiers().size());
                System.out.println("Survived: " + context.getSurvivedSoldiers().size());
                if (context.getSoldiers().size() == 0) {
                    System.exit(0);
                }

            }
        });
        return t1;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
