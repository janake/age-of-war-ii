package org.prodet.soldier;

import org.prodet.configuration.Context;
import org.prodet.configuration.Position3D;
import org.prodet.players.King;
import org.prodet.players.player1.Sith;
import org.prodet.players.player2.Jedi;
import org.prodet.soldier.formation.Squad;

import java.util.Set;

public class SoldiersFactory {

    public static void createJedi(Set<Soldier> soldiers, int number, int xPos, int yPos, String team, Context context,
                                  int minStamina, int minStrength, int minSpeed, String rank,
                                  Squad squad, King kingA, Position3D targetPos) {

        for (int i = 0; i < number; i++) {
            Position3D pos = squad.formation(number, i, xPos, yPos, 1, 5, 1, 0);
            Jedi jedi = new Jedi(pos, context, team, minStamina, minStrength, minSpeed, rank, kingA, targetPos);
            soldiers.add(jedi);
        }

    }

    public static void createSith(Set<Soldier> soldiers, int number, int xPos, int yPos, String team, Context context,
                                  int minStamina, int minStrength, int minSpeed, String rank,
                                  Squad squad, King kingB, Position3D targetPos) {

        for (int i = 0; i < number; i++) {
            Position3D pos = squad.formation(number, i, xPos, yPos, 1, 5, 1, 0);
            Sith sith = new Sith(pos, context, team, minStamina, minStrength, minSpeed, rank, kingB, targetPos);
            soldiers.add(sith);
        }

    }

    private static boolean containsKing(Set<Soldier> soldiers, String team){
        for( Soldier soldier : soldiers ){
            if(soldier instanceof King && soldier.getTeam().equals(team)){
                return  true;
            }
        }
        return  false;
    }
}