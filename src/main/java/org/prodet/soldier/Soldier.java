package org.prodet.soldier;

import org.prodet.configuration.Context;
import org.prodet.configuration.Names;
import org.prodet.configuration.Position3D;
import org.prodet.players.King;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

public abstract class Soldier extends Thread implements SoldierMoving {

    private final Context context;
    private final String team;
    private final int speed;
    private int strength;
    private int stamina;
    private final String soldierName;
    private final ReentrantLock lock = new ReentrantLock();
    private volatile int xPos;
    private volatile int yPos;
    private volatile int zPos = 1;
    protected volatile long id = UUID.randomUUID().getMostSignificantBits();
    private Position3D target = null;
    private King king;

    public Soldier(int xPos, int yPos, Context context, String team, int stamina,
                   int strength, int minSpeed, String rank, Position3D target, King king) {
        this.king = king;
        if (strength > 40 || stamina > 100) {
            System.out.println("Wrong properties: strength can't be higher than 40, stamina can't be higher than 100");
            System.exit(0);
        }
        this.xPos = xPos;
        this.yPos = yPos;
        this.context = context;
        this.team = team;
        this.speed = (int) (20 + (strength * 2.1 ));
        this.strength = 10 + strength;
        this.stamina = stamina;
        Names[] names = Names.values();
        this.soldierName = rank + names[Context.RND.nextInt(100)] +
                " " +
                names[Context.RND.nextInt(100)];
        this.target = target;
    }

    @Override
    public void run() {
        int lasted;
        while (isSoldierPresent()) {
            long start = System.currentTimeMillis();
            if (lock.tryLock()) {
                try {
                    Position3D nextPos = getNextPos();
                    if (isNextPosEmpty(nextPos)) {
                        tryToMoveToTheNextField(nextPos);
                    } else {
                        fightOrDoSomething(nextPos);
                    }
                    if (inTheFinish(new Position3D(xPos, yPos, zPos)) ||
                            Thread.currentThread().isInterrupted()) {
                        getRidOfSoldier();
                        context.addSurvivedSoldiers(this);
                    }
                } finally {
                    lock.unlock();
                }
            }
            lasted = (int) (System.currentTimeMillis() - start);
            try {
                Thread.sleep(Context.RND.nextInt(speed) + lasted);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    protected void messageAboutADeath() {}

    protected final void tryToMoveToTheNextField(Position3D nextPos) {
        if (isPositionValid(nextPos.getxPos(), nextPos.getyPos(), 1)) {
            moveToNextField(nextPos.getxPos(), nextPos.getyPos(), 1);
        } else {
            System.out.println(soldierName + " tried to move more than one!");
        }
    }

    protected final boolean isNextPosEmpty(Position3D nextPos) {
        return getBattleFieldValue(nextPos) == 0;
    }

    private final boolean isPositionValid(int nextXPos, int nextYPos, int nextZPos) {
        if ( (((Math.abs(nextXPos-xPos)) > 1) ||
                (Math.abs(nextYPos-yPos) > 1) ||
                (Math.abs(nextZPos-zPos) > 1)) && context.existingPosition(xPos, yPos, zPos)) {
            return false;
        }
        return true;
    }

    private final Position3D getNextPos() {
        return move(xPos, yPos, zPos, 1);
    }

    private final void getRidOfSoldier() {
        lock.lock();
        try {
            context.removeSoldier(this);
            context.setValueOnBattleField(xPos, yPos, 0);
        } finally {
            lock.unlock();
        }
    }

    private final boolean isSoldierPresent() {
        return getSoldierById(id).isPresent();
    }

    private final void isHit(int hitStrength) {
        stamina -= hitStrength;
        if (stamina <= 0) {
            messageAboutADeath();
            getRidOfSoldier();
            ifIDied();
        }
    }

    protected void ifIDied() {
        //message to relatives or something similar
    }

    private final void moveToNextField(int nextXPos, int nextYPos, int nextZPos) {
        lock.lock();
        try {
            context.setValueOnBattleField(xPos, yPos, 0);
            context.setValueOnBattleField(nextXPos, nextYPos, id);
            this.xPos = nextXPos;
            this.yPos = nextYPos;
        } finally {
            lock.unlock();
        }
    }
    protected final void fightForPos(Soldier soldier) {
        if (soldier.lock.tryLock()) {
            try {
                soldier.isHit(this.strength);
            } finally {
                soldier.lock.unlock();
            }
        }
    }

    private final void fightOrDoSomething(Position3D nextPos) {
        lock.tryLock();
        try {
            long targetFieldId = getBattleFieldValue(nextPos);
            Optional<Soldier> soldierById = getSoldierById(targetFieldId);
            if (isBattleFieldPosEmpty(targetFieldId) && soldierById.isPresent()) {
                Soldier soldierOnTargetField = soldierById.get();
                if (soldierOnTargetField.lock.tryLock()) {
                    try {
                        //TODO: later need to check whether this is enemy or object
                        if (!isTeamMate(soldierOnTargetField)) {
                            enemyIsOnTargetPos(soldierOnTargetField);
                        } else {
                            friendIsOnTargetPos(soldierOnTargetField);
                        }
                    } finally {
                        soldierOnTargetField.lock.unlock();
                    }
                }
            }
        } finally {
            lock.unlock();
        }
    }

    protected void enemyIsOnTargetPos(Soldier soldierOnTargetField) {
        fightForPos(soldierOnTargetField);
    }

    protected void friendIsOnTargetPos(Soldier soldierOnNextField) {

    }

    protected final long getBattleFieldValue(Position3D nextPos) {
        lock.lock();
        try {
            return context.getBattleFieldValue(nextPos.getxPos(), nextPos.getyPos());
        } finally {
            lock.unlock();
        }
    }

    protected final Optional<Soldier> getSoldierById(long soldierTargetFieldId) {
        lock.lock();
        try {
            return context.getSoldierById(soldierTargetFieldId);
        } finally {
            lock.unlock();
        }
    }

    public Position3D move(int posX, int posY, int posZ, int speed) {

        if(target.getxPos() == this.getxPos() && target.getyPos()>this.getyPos()){
            posY++;
        }
        else if(target.getxPos() == this.getxPos() && target.getyPos()<this.getyPos()){
            posY--;
        }
        else if (target.getxPos() > posX) {
            posX += 1;
        } else {
            posX -= 1;
        }

        return new Position3D(posX, posY, posZ);
    }

    protected void teamMateIsOnNextField(Soldier soldierOnNextField) {}

    protected final boolean isTeamMate(Soldier soldier) {
        lock.lock();
        try {
            return soldier.getTeam().equals(team);
        } finally {
            lock.unlock();
        }
    }

    public final String getTeam() {
        return team;
    }

    private final boolean isBattleFieldPosEmpty(long soldierOnNextFieldId) {
        return context.getSoldierById(soldierOnNextFieldId).isPresent();
    }

    public int getStamina() {
        return stamina;
    }

    public final int getxPos() {
        return xPos;
    }

    public final int getyPos() {
        return yPos;
    }

    public final long getId() {
        return id;
    }

    protected final Position3D getTarget() {
        return target;
    }

    protected void setTarget(Position3D target) {
        this.target = target;
    }

    protected final String getSoldierName() {
        return soldierName;
    }

    protected final boolean isMyKingDied() {
        return king.getStamina() < 0;
    }
}
