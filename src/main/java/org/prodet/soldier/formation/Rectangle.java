package org.prodet.soldier.formation;

import org.prodet.configuration.Position3D;

public class Rectangle implements Squad {

    int soldierPerLine;

    public Rectangle(int soldierPerLine){
        this.soldierPerLine = soldierPerLine;
    }

    @Override
    public Position3D formation(int numberOfSoldiers, int numberOfPosition,
                                int xPos, int yPos, int zPos, int xOffset, int yOffset, int zOffset) {
        int x;
        int y;
        setkSoldiersPerLine(numberOfSoldiers);
        y = yPos + (numberOfPosition-(numberOfPosition / soldierPerLine)*soldierPerLine)*yOffset;
        x = xPos + (numberOfPosition / soldierPerLine * xOffset);

        return new Position3D(x, y, 1);
    }

    private void setkSoldiersPerLine(int numberOfSoldiers){
        if(this.soldierPerLine > numberOfSoldiers){
            this.soldierPerLine= numberOfSoldiers;
        }
    }
}
