package org.prodet.soldier.formation;

import org.prodet.configuration.Position3D;

public class Square implements Squad {

    @Override
    public Position3D formation(int numberOfSoldiers, int numberOfPosition,
                                int xPos, int yPos, int zPos, int xOffset, int yOffset, int zOffset) {
        int x = xPos + ((int)(numberOfPosition % Math.sqrt(numberOfSoldiers)) * xOffset);
        int y = yPos + ((int)(numberOfPosition / Math.sqrt(numberOfSoldiers)) * yOffset);
        return new Position3D(x, y, 1);
    }
}