package org.prodet.soldier.formation;

import org.prodet.configuration.Position3D;

public interface Squad {

    Position3D formation(int numberOfSoldiers, int numberOfPosition, int xPos, int yPos, int zPos, int xOffset, int yOffset, int zOffset);
}
