package org.prodet.soldier.formation;

import org.prodet.configuration.Position3D;

import java.util.ArrayList;
import java.util.List;

public class Triangle implements Squad {

    @Override
    public Position3D formation(int numberOfSoldiers, int numberOfPosition,
                    int xPos, int yPos, int zPos, int xOffset, int yOffset, int zOffset) {

        int soldiersNumberInColumn = 1;
        int soldiersInSquad = 1;
        int biggestColumn = 0;
        List<Integer> columns = new ArrayList<>();
        columns.add(1);

        while (soldiersInSquad < numberOfSoldiers) {
            soldiersNumberInColumn += 2;
            soldiersInSquad += soldiersNumberInColumn;
            biggestColumn = getBiggestColumn(soldiersNumberInColumn, biggestColumn);
            columns.add(soldiersNumberInColumn);
        }

        int round = 0;
        int currentNumberOfPosition = 0;
        int x = 0;
        int y = 0;
        for (int column: columns) {
            round++;
            for (int i = -(column)/2; i < (column+1)/2; i++) {
                if (currentNumberOfPosition++ == numberOfPosition) {
                    x = xPos + (round * xOffset);
                    y = yPos + ((biggestColumn/2) * yOffset) + (i * yOffset);
                }
            }
        }

        return new Position3D(x, y, 1);
    }

    private int getBiggestColumn(int soldiersInColumn, int biggestColumn) {
        if (soldiersInColumn > biggestColumn) {
            biggestColumn = soldiersInColumn;
        }
        return biggestColumn;
    }

}
