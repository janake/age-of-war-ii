package org.prodet.soldier;

import org.prodet.configuration.Position3D;

public interface SoldierMoving {

    public Position3D move(int posX, int posY, int posZ, int speed);
    public boolean inTheFinish(Position3D pos);

}
